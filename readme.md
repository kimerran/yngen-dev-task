
# Yngen Dev Task - Mark Hugh Neri

## To install and run
```
npm install
npm start

Optional (enable debug logs)
npm run start:debug
```

## Usage
1. Visit http://localhost:3000 in your browser
2. Enter the URL to shorten in the input box
3. Click on `Shorten`
4. Results will be displayed below


## Backend endpoint
```
POST /shorten

payload:
{
  "long_url" : <LONG URL TO SHORTEN>
}

```

## Configuration
You can override the default Bit.ly username and password using the ff environment variables
```
BITLY_USERNAME=<your own username>
BITLY_PASSWORD=<your own password>
```
