const debug = require('debug')('yngentest');
const express = require('express');
const bodyParser = require('body-parser')

const BitlyApi = require('./api/bitly.api');

const app = express();
app.use(bodyParser.json())

const bitlyApiService = new BitlyApi();

/**
 * main handler for the URL shortener
 * payload expected is `long_url`
 */
app.post('/shorten', async (req, res) => {
  debug('/shorten endpoint called');
  const { long_url } = req.body;

  debug('long_url', long_url);
  debug('calling shortenUrl service');
  const shortenResponse = await bitlyApiService.shortenUrl(long_url);

  debug('shortenUrl response', shortenResponse);

  if (shortenResponse.err) {
    debug('shortenResponse err', shortenResponse.err);
    res.status(400).json(shortenResponse);
  } else {
    debug('shortenResponse success', shortenResponse);
    res.json(shortenResponse);
  }
})

/**
 * For serving the sample tester UI
 */
app.use('/', express.static('public'));

module.exports = app;
