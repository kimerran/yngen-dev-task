const debug = require('debug')('yngentest');
const app = require('./server');

debug('starting express app');
app.listen(3000, () => { 
  console.log('App listening http://localhost:3000')
});
