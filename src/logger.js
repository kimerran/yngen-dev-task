const debug = require('debug')('yngentest');
const winston = require('winston');

/**
 * Helper utility for logging
 * making use of winston file transport
 */
class Logger {
  constructor() {
    this.logger = winston.createLogger({
      transports: [
        new winston.transports.File({ filename: 'tmp/app.log' }),
      ]
    });
  }

  info(message) {
    this.logger.info(`${new Date()} - ${message}`);
  }
}

module.exports = Logger;
