const debug = require('debug')('yngentest');
const axios = require('axios');

const Logger = require('../logger');

const BITLY_API_ROOT_URL = 'https://api-ssl.bitly.com';
const ACCESS_TOKEN_ENDPOINT = `${BITLY_API_ROOT_URL}/oauth/access_token`;
const SHORTEN_ENDPOINT = `${BITLY_API_ROOT_URL}/shorten`;

const bitlyUsername = process.env.BITLY_USERNAME || 'devtaskmarkneri';
const bitlyPassword = process.env.BITLY_PASSWORD || 'devtaskmarkneri2019';

class BitlyApi {
  constructor() {
    this.logger = new Logger();
  }
  /**
   * Helper function to generate access token to be used for the Bit.ly API
   */
  async generateAccessToken() {
    debug('generating access token username', bitlyUsername);
    debug('generating access token password', bitlyPassword);
    try {
      const { data } = await axios.post(`${ACCESS_TOKEN_ENDPOINT}`, {}, {
        auth: {
          username: bitlyUsername,
          password: bitlyPassword,
        }
      });

      /**
       * If request is successful, Bit.ly will return a string response containing
       * the access token
       */
      debug('access token response', data);
      if (typeof data === 'string') {
        return data;
      } else {
        return undefined;
      }
    } catch (error) {
      return undefined;
    }
  }

  /**
   * 
   * @param {*} longUrl 
   */
  async shortenUrl(longUrl) {
    const token = await this.generateAccessToken();
    debug('token generated', token);
    try {
      /**
       * If token is present, we proceed,
       * otherwise we return a generic error that we are unable to shorten url
       */
      if (token) {
        const longUrlEncoded = encodeURIComponent(longUrl);
        debug('longUrlEncoded', longUrlEncoded);

        const shortenUrlEndpoint = `${SHORTEN_ENDPOINT}?access_token=${token}&longUrl=${longUrlEncoded}`;
        
        debug('requesting shorten', shortenUrlEndpoint);
        const { data } = await axios.get(`${shortenUrlEndpoint}`);
        const { shortUrl } = data.results[longUrl];

        debug('shortUrl success', shortUrl);
  
        this.logger.info(`${longUrl} - ${shortUrl}`);
        return shortUrl;
      }
  
      debug('Unable to shorten url');
      return { err: 'Unable to shorten url' };
    } catch (error) {
      debug(error);
      debug('Unable to shorten url');
      return { err: 'Unable to shorten url'};
    }

  }
}

module.exports = BitlyApi;
